package com.techinsight.autism;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by alex on 11/2/15.
 */
public class MyWearableListenerService extends WearableListenerService {

    public static final String START_ACTIVITY = "/START_HEART_RATE";
    public static final String STOP_ACTIVITY = "/STOP_HEART_RATE";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( START_ACTIVITY ) ) {
            Log.i("WearableListener", START_ACTIVITY);
            Intent intent = new Intent( this, MyActivity.class );
            intent.setAction(START_ACTIVITY);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity( intent );
        } else if( messageEvent.getPath().equalsIgnoreCase( STOP_ACTIVITY ) ) {
            Log.i("WearableListener", STOP_ACTIVITY);
            Intent intent = new Intent( this, MyActivity.class );
            intent.setAction(STOP_ACTIVITY);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity( intent );
        } else {
            super.onMessageReceived( messageEvent );
        }
    }
}
