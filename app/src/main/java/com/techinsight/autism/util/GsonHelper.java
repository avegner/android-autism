package com.techinsight.autism.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

/**
 * Created by alex on 9/11/14.
 */
public class GsonHelper {

    private static Gson gsonInstance;

    public static <E> E loadModelFromAssets(Context context, String fileName, E newEmpty) {
        Gson gson = getGson();
        E result = null;
        AssetManager assetManager = context.getAssets();
        try {
            InputStream inputStream  = assetManager.open(fileName);
            JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
            result = gson.fromJson(reader, newEmpty.getClass());
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <E> E loadModelFromInternalStorage(Context context, String fileName, E newEmpty) {
        Gson gson = getGson();
        E result = newEmpty;
        File file = new File(context.getFilesDir(), fileName);
        if ( file.exists() ) {
            // loadWeb from Json
            FileInputStream inputStream = null;
            try {
                inputStream  = context.openFileInput(fileName);
                JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));
                result = gson.fromJson(reader, newEmpty.getClass());
                inputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static <E> void saveFileToInternalStorage(Context context, String fileName, E entity) {
        File file = new File(context.getFilesDir(), fileName);
        if ( file.exists() ) {
            file.delete();
        }
        // save to Json
        try {
            OutputStreamWriter outputStreamWriter  = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            String json = getJsonFromObject(entity);
            LogUtils.LOGT(GsonHelper.class, json);
            outputStreamWriter.write(json);
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) { //
            e.printStackTrace();
        }
    }

    public static <E> String getJsonFromObject(E entity){
        Gson gson = getGson();
        return gson.toJson(entity);
    }

    public static <E> String getJsonFromObject(E entity, Type type){
        Gson gson = getGson();
        return gson.toJson(entity, type);
    }

    public static <E> E parseObjectFromJson(String json, E newEntity){
        if (json == null)
            return null;
        Gson gson = getGson();
        E result = null;
        try {
            result = (E) gson.fromJson(json, newEntity.getClass());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <E> E parseObjectFromJson(String json, Type type){
        if (json == null)
            return null;
        Gson gson = getGson();
        E result = null;
        try {
            result = (E) gson.fromJson(json, type);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Nullable
    public static <E> E deepCopy(E entity) {
        if (entity == null)
            return null;
        Gson gson = getGson();
        String json = gson.toJson(entity);
        return (E) gson.fromJson(json, entity.getClass());
    }

    @Nullable
    public static <E> E deepCopy(E entity, Type type) {
        if (entity == null || type == null)
            return null;
        Gson gson = getGson();
        String json = gson.toJson(entity, type);
        return (E) gson.fromJson(json, type);
    }

    public static <E> boolean compareObjects(E entity1, E entity2) {
        String entity1Str = getJsonFromObject(entity1);
        String entity2Str = getJsonFromObject(entity2);
        return entity1Str.equals(entity2Str);
    }

    private static Gson getGson(){
        if (gsonInstance == null){
            gsonInstance = new GsonBuilder().serializeNulls().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        }
        return gsonInstance;

        /*GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            @Override
            public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                    throws JsonParseException {
                try {
                    return df.parse(json.getAsString());
                } catch (ParseException e) {
                    return null;
                }
            }
        });
        gsonBuilder.serializeNulls();
        return gsonBuilder.create();*/
    }
}
