package com.techinsight.autism.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by alex on 9/8/14.
 */
public class DayLogHelper {

    private static final String SIMPLE_DAY_DATE_FORMAT_STRING = "yyyy-MM-dd";
    private static final String DAY_DATE_FORMAT_STRING = "dd";
    private static final String MONTH_DATE_FORMAT_STRING = "MM";
    private static final String YEAR_DATE_FORMAT_STRING = "yyyy";
    private static final String SIMPLE_DAY_DATE_SLASH_FORMAT_STRING = "MM/dd/yyyy";
    public static final String TIMESTAMP_DAY_DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";
    private static SimpleDateFormat SIMPLE_DAY_DATE_FORMAT =  new SimpleDateFormat(SIMPLE_DAY_DATE_FORMAT_STRING);
    private static SimpleDateFormat DAY_DATE_FORMAT =  new SimpleDateFormat(DAY_DATE_FORMAT_STRING);
    private static SimpleDateFormat MONTH_DATE_FORMAT =  new SimpleDateFormat(MONTH_DATE_FORMAT_STRING);
    private static SimpleDateFormat YEAR_DATE_FORMAT =  new SimpleDateFormat(YEAR_DATE_FORMAT_STRING);
    private static SimpleDateFormat SIMPLE_DAY_DATE_SLASH_FORMAT =  new SimpleDateFormat(SIMPLE_DAY_DATE_SLASH_FORMAT_STRING);
    private static SimpleDateFormat TIMESTAMP_DAY_DATE_FORMAT =  new SimpleDateFormat(TIMESTAMP_DAY_DATE_FORMAT_STRING);

    public static Date getCurrentDateYMD(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getPrevDay(Date date, int minus){
        if (date == null)
            return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -minus);
        return calendar.getTime();
    }

    public static Date getPrevDay(Date date){
        return getPrevDay(date, 1);
    }

    public static Date getNextDay(Date date, int plus){
        if (date == null)
            return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, plus);
        return calendar.getTime();
    }

    public static Date getNextDay(Date date){
        return getNextDay(date, 1);
    }

    public static boolean equalDayDate(Date date1, Date date2) {
        String strDate1 = dayDateToString(date1);
        String strDate2 = dayDateToString(date2);
        return strDate1.equals(strDate2);
    }

    public static String dayDateToString(Date date) {
        // fix for HW360-159NullPointerException - utils.DayLogHelper.dayDateToString
        if (date == null)
            return "";
        // ---
        return SIMPLE_DAY_DATE_FORMAT.format(date);
    }

    public static String dayLongToString(long time) {
        return SIMPLE_DAY_DATE_FORMAT.format(new Date(time));
    }

    public static String timeLongToString(long time) {
        return TIMESTAMP_DAY_DATE_FORMAT.format(new Date(time));
    }

    public static String dayDateToSlashString(Date date) {
        return SIMPLE_DAY_DATE_SLASH_FORMAT.format(date);
    }

    public static Date stringToSlashDayDate(String strDate) {
        Date date = null;
        try {
            date = SIMPLE_DAY_DATE_SLASH_FORMAT.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date stringToDayDate(String strDate) {
        Date date = null;
        try {
            date = SIMPLE_DAY_DATE_FORMAT.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date timestampToDate(String strDate) {
        Date date = null;
        try {
            date = TIMESTAMP_DAY_DATE_FORMAT.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String dateDateToTimestamp(Date date) {
        return TIMESTAMP_DAY_DATE_FORMAT.format(date);
    }

    public static String getDateDay(Date date) {
        return DAY_DATE_FORMAT.format(date);
    }

    public static String getDateMonth(Date date) {
        return MONTH_DATE_FORMAT.format(date);
    }

    public static String getDateYear(Date date) {
        return YEAR_DATE_FORMAT.format(date);
    }

    public static Date maxDate(Date d1, Date d2) {
        if (d1 == null)
            return d2;
        else if (d2 == null)
            return d1;
        else
            return d1.compareTo(d2) > 0 ? d1 : d2;
    }

    public static Date minDate(Date d1, Date d2) {
        if (d1 == null)
            return d2;
        else if (d2 == null)
            return d1;
        else
            return d1.compareTo(d2) < 0 ? d1 : d2;
    }

    public static String longDateToTimestamp(long time) {
        return dateDateToTimestamp(new Date(time));
    }
}