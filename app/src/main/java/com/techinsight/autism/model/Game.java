package com.techinsight.autism.model;

import com.techinsight.autism.R;
import com.techinsight.autism.util.TimeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 10/30/15.
 */
public class Game {
    private long startTime;
    private long duration;
    private List<Question> questions;
    private int currentQuestion;
    private int questionCount;
    private boolean competed;
    private List<HardRate> hardRateList;

    public Game() {}

    public Game(long startTime, long duration, List<Question> questions, int currentQuestion, int questionCount, boolean isCompleted) {
        this.startTime = startTime;
        this.duration = duration;
        this.questions = questions;
        this.currentQuestion = currentQuestion;
        this.questionCount = questionCount;
        this.competed = isCompleted;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public void setCurrentQuestion(int currentQuestion) {
        this.currentQuestion = currentQuestion;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public static Game makeFake() {
        Game game = new Game();
        game.startTime = System.currentTimeMillis();
        game.currentQuestion = 0;
        game.questionCount = 3;
        game.questions = new ArrayList<>(game.questionCount);

        Question q1 = new Question();
        q1.setImage(R.drawable.fox_face_happiness_predator_92056_1024x768);
        q1.setAnswer1("Lion");
        q1.setAnswer2("Fox");
        q1.setAnswer3("Cat");
        q1.setAnswer4("Mouse");
        q1.setCorrectAnswer("Fox");
        game.questions.add(q1);


        Question q2 = new Question();
        q2.setImage(R.drawable.howling_wolf_crying_dog_77071_1024x768);
        q2.setAnswer1("Fox");
        q2.setAnswer2("Cat");
        q2.setAnswer3("Wolf");
        q2.setAnswer4("Dog");
        q2.setCorrectAnswer("Wolf");
        game.questions.add(q2);

        Question q3 = new Question();
        q3.setImage(R.drawable.owl_bird_predator_look_95022_1024x768);
        q3.setAnswer1("Fox");
        q3.setAnswer2("Cat");
        q3.setAnswer3("Wolf");
        q3.setAnswer4("Owl");
        q3.setCorrectAnswer("Owl");
        game.questions.add(q3);

        return game;
    }

    public Question nextQuestion() {
        if (isCompeted()) {
            return null;
        } if (currentQuestion+1 >= questionCount) {
            currentQuestion++;
            competed = true;
            duration = TimeUtil.time() - startTime;
            return null;
        } else {
            if (currentQuestion > -1) {
                question().getQuestionResult().setDuration(TimeUtil.time() - question().getQuestionResult().getStartTime());
            }
            currentQuestion++;
            return questions.get(currentQuestion);
        }
    }

    public Question question() {
        return questions.get(currentQuestion);
    }

    public boolean isCompeted() {
        return competed;
    }

    public List<HardRate> getHardRateList() {
        return hardRateList;
    }

    public void setHardRateList(List<HardRate> hardRateList) {
        this.hardRateList = hardRateList;
    }
}
