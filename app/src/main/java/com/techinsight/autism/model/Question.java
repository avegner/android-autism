package com.techinsight.autism.model;

/**
 * Created by alex on 10/30/15.
 */
public class Question {
    private int image;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String correctAnswer;
    private QuestionResult questionResult;

    public Question() {}

    public Question(int image, String answer1, String answer2, String answer3, String answer4, String correctAnswer, QuestionResult questionResult) {
        this.image = image;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.correctAnswer = correctAnswer;
        this.questionResult = questionResult;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public QuestionResult getQuestionResult() {
        if (questionResult == null) {
            questionResult = new QuestionResult();
        }
        return questionResult;
    }

    public void setQuestionResult(QuestionResult questionResult) {
        this.questionResult = questionResult;
    }
}
