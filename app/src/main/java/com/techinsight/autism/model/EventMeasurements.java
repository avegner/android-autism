package com.techinsight.autism.model;

/**
 * Created by alex on 10/31/15.
 */
public class EventMeasurements {
    private int aggressionToSelf;
    private int aggressionToOther;
    private int happiness;

    public EventMeasurements() {
    }

    public EventMeasurements(int aggressionToSelf, int aggressionToOther, int happiness) {
        this.aggressionToSelf = aggressionToSelf;
        this.aggressionToOther = aggressionToOther;
        this.happiness = happiness;
    }

    public int getAggressionToSelf() {
        return aggressionToSelf;
    }

    public void setAggressionToSelf(int aggressionToSelf) {
        this.aggressionToSelf = aggressionToSelf;
    }

    public int getAggressionToOther() {
        return aggressionToOther;
    }

    public void setAggressionToOther(int aggressionToOther) {
        this.aggressionToOther = aggressionToOther;
    }

    public int getHappiness() {
        return happiness;
    }

    public void setHappiness(int happiness) {
        this.happiness = happiness;
    }
}
