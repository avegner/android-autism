package com.techinsight.autism.model;

/**
 * Created by alex on 11/3/15.
 */
public class HardRate {
    private int value;
    private long time;

    public HardRate() {
    }

    public HardRate(int value, long time) {
        this.value = value;
        this.time = time;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
