package com.techinsight.autism.model;

/**
 * Created by alex on 10/30/15.
 */
public class QuestionResult {
    private int triesCount;
    private long startTime;
    private long duration;

    public QuestionResult(){}

    public QuestionResult(int triesCount, long startTime, long duration) {
        this.triesCount = triesCount;
        this.startTime = startTime;
        this.duration = duration;
    }

    public int getTriesCount() {
        return triesCount;
    }

    public void setTriesCount(int triesCount) {
        this.triesCount = triesCount;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void incTriesCount() {
        this.triesCount++;
    }
}
