package com.techinsight.autism.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.techinsight.autism.util.DayLogHelper;
import com.techinsight.autism.util.TimeUtil;

import java.util.Date;
import java.util.UUID;

/**
 * Created by alex on 10/30/15.
 */
public class Event {

    public static String[] TYPE_STRINGS = {"GAME", "NOTES", "MEASUREMENTS"};
    public static long sUUIDHash = 0;

    private int type = 0;
    private long time;
    private Game game;
    private String id;
    private String note;
    private EventMeasurements measurements;
    private String doctorComment;

    public Event() {
    }

    public Event(int type, long time, Game game, String id) {
        this.type = type;
        this.time = time;
        this.game = game;
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    private String getDateStr(){
        return DayLogHelper.dayDateToString(new Date(time));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Event makeNew(Context context){
        Event event = new Event();
        event.generateId(context);
        return event;
    }

    public String generateId(Context context) {
        //SharedPreferences.
        if (sUUIDHash == 0) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            long uuidHash = prefs.getLong("sUUIDHash", 0);
            if (uuidHash == 0) {
                sUUIDHash = new  UUID(123, System.currentTimeMillis()).hashCode();
                prefs.edit().putLong("sUUIDHash", sUUIDHash).commit();
            }
        }

        id = new UUID(sUUIDHash, TimeUtil.time()).toString();
        return id;
    }


    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public EventMeasurements getMeasurements() {
        if (measurements == null) {
            measurements = new EventMeasurements();
        }
        return measurements;
    }

    public void setMeasurements(EventMeasurements measurements) {
        this.measurements = measurements;
    }

    public String getDoctorComment() {
        return doctorComment;
    }

    public void setDoctorComment(String doctorComment) {
        this.doctorComment = doctorComment;
    }
}
