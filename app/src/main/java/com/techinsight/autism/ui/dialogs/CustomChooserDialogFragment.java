package com.techinsight.autism.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;


/**
 * Created by alex on 10/4/14.
 */
public class CustomChooserDialogFragment extends DialogFragment {

    private OnDateSetListener mCallback;

    public static CustomChooserDialogFragment newInstance(String title, String[] values) {
        CustomChooserDialogFragment frag = new CustomChooserDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putStringArray("values", values);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), com.techinsight.autism.R.style.MyAlertDialogStyle);
        builder.setTitle(getArguments().getString("title"))
                .setItems(getArguments().getStringArray("values"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDateSet(which);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });

        return builder.create();
    }

    public void setOnDataSetListener(OnDateSetListener callback){
        mCallback = callback;
    }

    /**
     * The callback interface
     */
    public interface OnDateSetListener {
        void onDateSet(int position);
    }

    public static void showDialog(FragmentManager fragmentManager, String title, String[] values, OnDateSetListener callback) {
        String dialogTag = CustomChooserDialogFragment.class.getSimpleName();
        if (fragmentManager.findFragmentByTag(dialogTag) == null){
            CustomChooserDialogFragment dialogFragment = CustomChooserDialogFragment.newInstance(title, values);
            dialogFragment.show(fragmentManager, dialogTag);
            dialogFragment.setOnDataSetListener(callback);
        }
    }

    private void close(){
        dismiss();
    }

}
