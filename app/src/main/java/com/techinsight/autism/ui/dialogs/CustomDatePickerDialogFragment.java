package com.techinsight.autism.ui.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.widget.DatePicker;


import com.techinsight.autism.R;

import java.util.Calendar;

/**
 * Created by alex on 8/20/15.
 */
public class CustomDatePickerDialogFragment extends DialogFragment {

    private OnDateSetListener mCallback;
    private DatePicker mDatePicker;

    public static CustomDatePickerDialogFragment newInstance(long selectedDateMs) {
        CustomDatePickerDialogFragment frag = new CustomDatePickerDialogFragment();
        Bundle args = new Bundle();
        args.putLong("selectedDateMs", selectedDateMs);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        Calendar calendar =  Calendar.getInstance();
        calendar.setTimeInMillis(getArguments().getLong("selectedDateMs"));
        mDatePicker = new DatePicker(getActivity());
        mDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), null);
        //mDatePicker.setMinDate(System.currentTimeMillis() - 1000);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(getArguments().getLong("selectedDateMs"));
                calendar.set(Calendar.YEAR, mDatePicker.getYear());
                calendar.set(Calendar.MONTH, mDatePicker.getMonth());
                calendar.set(Calendar.DATE, mDatePicker.getDayOfMonth());
                mCallback.onDateSet(calendar.getTimeInMillis());
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.setView(mDatePicker);
        return alertDialog;
    }

    public void setOnDataSetListener(OnDateSetListener callback){
        mCallback = callback;
    }

    /**
     * The callback interface
     */
    public interface OnDateSetListener {
        void onDateSet(long timeMs);
    }

    public static void showDialog(FragmentManager fragmentManager, long selectedDateMs, OnDateSetListener callback) {
        String dialogTag = CustomDatePickerDialogFragment.class.getSimpleName();
        if (fragmentManager.findFragmentByTag(dialogTag) == null){
            CustomDatePickerDialogFragment dialogFragment = CustomDatePickerDialogFragment.newInstance(selectedDateMs);
            dialogFragment.show(fragmentManager, dialogTag);
            dialogFragment.setOnDataSetListener(callback);
        }
    }

    private void close(){
        dismiss();
    }

}
