package com.techinsight.autism.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import com.techinsight.autism.model.Event;
import com.techinsight.autism.model.Game;
import com.techinsight.autism.ui.dialogs.CustomAlertDialogFragment;
import com.techinsight.autism.ui.fragment.GameFragment;
import com.techinsight.autism.ui.fragment.PlayFragment;
import com.techinsight.autism.util.DayLogHelper;
import com.techinsight.autism.util.TimeUtil;

/**
 * Created by alex on 10/30/15.
 */
public class UserActivity extends BaseActivity implements PlayFragment.PlayGameListener, GameFragment.GameListener {


    public static Intent getStartIntent(Context context){
        Intent intent = new Intent(context, UserActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.techinsight.autism.R.layout.user_activity);
        Firebase.setAndroidContext(this);
        enterPlay();

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        Firebase myFirebaseRef = new Firebase("https://autism.firebaseio.com");
        myFirebaseRef.child("EVENTS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                System.out.println(snapshot.getValue());  //prints "Do you have data? You'll love Firebase."
            }
            @Override public void onCancelled(FirebaseError error) { }
        });
    }

    private void enterPlay() {
        Fragment playFragment = PlayFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(com.techinsight.autism.R.id.container, playFragment, "PlayFragment")
                .commit();
    }


    @Override
    public void startGame() {
        Fragment gameFragment = GameFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(com.techinsight.autism.R.id.container, gameFragment, "GameFragment")
                .addToBackStack("GameFragment")
                .commit();
        /*BlogPost blogPost = new BlogPost();
        blogPost.title = "sdf";
        blogPost.author = "sdf";

        Game game = Game.makeFake();
        game.setStartTime(TimeUtil.time());

        Event event = new Event();
        event.setType(0);
        event.setGame(game);


        Firebase myFirebaseRef = new Firebase("https://autism.firebaseio.com/TEST/");
        myFirebaseRef
                .child("bb" + new Random(TimeUtil.time()).nextInt(100))
                .setValue(event);*/
    }

    @Override
    public void endGame(Game game) {
        CustomAlertDialogFragment.showDialog(getSupportFragmentManager(), "Game completed", "Congratulate, you win!");
        saveResult(game);

    }

    private void saveResult(Game game) {
        Event event = Event.makeNew(getApplicationContext());
        event.setGame(game);
        event.setTime(TimeUtil.time());
        event.setType(0);

        Firebase myFirebaseRef = new Firebase("https://autism.firebaseio.com/EVENTS/");

        myFirebaseRef
        .child(DayLogHelper.dayLongToString(event.getTime()))
                .child(event.getId())
                .setValue(event);
    }

    public static class BlogPost {
        private String author;
        private String title;
        public BlogPost() {
            // empty default constructor, necessary for Firebase to be able to deserialize blog posts
        }
        public String getAuthor() {
            return author;
        }
        public String getTitle() {
            return title;
        }
    }




}
