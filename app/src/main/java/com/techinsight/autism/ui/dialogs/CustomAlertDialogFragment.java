package com.techinsight.autism.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by alex on 12/22/14.
 */
public class CustomAlertDialogFragment extends DialogFragment {

    public static CustomAlertDialogFragment newInstance(String title, String message) {
        CustomAlertDialogFragment frag = new CustomAlertDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        //set Text
        if (title == null) {
            title = getString(android.R.string.dialog_alert_title);
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dismiss();
                            }
                        }
                )
                .create();
    }

    public static void showDialog(android.support.v4.app.FragmentManager fragmentManager, String title, String message) {
        showDialog(fragmentManager, title, message, null);
    }

    public static void showDialog(android.support.v4.app.FragmentManager fragmentManager, String title, String message, String fragmentTag) {
        DialogFragment newFragment = CustomAlertDialogFragment.newInstance(title, message);
        if (fragmentTag == null)
            fragmentTag = newFragment.getClass().getSimpleName()+message;
        newFragment.show(fragmentManager, fragmentTag);
    }


 }