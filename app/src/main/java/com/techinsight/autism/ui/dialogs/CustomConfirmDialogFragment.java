package com.techinsight.autism.ui.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.TimePicker;


/**
 * Created by alex on 8/20/15.
 */
public class CustomConfirmDialogFragment extends DialogFragment {

    private Listener mCallback;
    private TimePicker mTimePicker;

    public static CustomConfirmDialogFragment newInstance(String title, String message) {
        CustomConfirmDialogFragment frag = new CustomConfirmDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), com.techinsight.autism.R.style.MyAlertDialogStyle);
        builder.setTitle(getArguments().getString("title"));
        builder.setMessage(getArguments().getString("message"));
        builder.setPositiveButton(com.techinsight.autism.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCallback.onAccept();
            }
        });
        builder.setNegativeButton(com.techinsight.autism.R.string.cancel, null);
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.setView(mTimePicker);
        return alertDialog;
    }

    public void setOnDataSetListener(Listener callback){
        mCallback = callback;
    }

    /**
     * The callback interface
     */
    public interface Listener {
        void onAccept();
    }

    public static void showDialog(FragmentManager fragmentManager, String title, String message, Listener callback) {
        String dialogTag = CustomConfirmDialogFragment.class.getSimpleName();
        if (fragmentManager.findFragmentByTag(dialogTag) == null){
            CustomConfirmDialogFragment dialogFragment = CustomConfirmDialogFragment.newInstance(title, message);
            dialogFragment.show(fragmentManager, dialogTag);
            dialogFragment.setOnDataSetListener(callback);
        }
    }

    private void close(){
        dismiss();
    }

}
