package com.techinsight.autism.ui.activity.events;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.techinsight.autism.R;
import com.techinsight.autism.model.Event;
import com.techinsight.autism.util.DayLogHelper;
import com.techinsight.autism.util.GsonHelper;
import com.techinsight.autism.util.TimeUtil;


/**
 * A fragment representing a single Event detail screen.
 * This fragment is either contained in a {@link EventListActivity}
 * in two-pane mode (on tablets) or a {@link EventDetailActivity}
 * on handsets.
 */
public class EventDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String EVENT_EXT = "EVENT_EXT";

    /**
     * The dummy content this fragment is presenting.
     */
    private Event mEvent;
    private int mUserType;
    private boolean mTwoPanel;
    private EditText mDoctorCommentText;
    private EditText mNoteText;
    private boolean isNewEvent;
    private TextView mGameText;
    private RatingBar mReatingAggressionToSelf;
    private RatingBar mReatingAggressionToOther;
    private RatingBar mReatingHappinse;
    private Callback mCallback;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventDetailFragment() {
    }

    public static EventDetailFragment newInstance(String eventJson, int userType, boolean twoPanel) {
        Bundle args = new Bundle();
        args.putString(EVENT_EXT, eventJson);
        args.putInt("userType", userType);
        args.putBoolean("twoPanel", twoPanel);
        EventDetailFragment fragment = new EventDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(EVENT_EXT)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mEvent = GsonHelper.parseObjectFromJson(getArguments().getString(EVENT_EXT), new Event());

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(Event.TYPE_STRINGS[mEvent.getType()]);
            }
            isNewEvent = TextUtils.isEmpty(mEvent.getId());
            if (isNewEvent) {
                mEvent.generateId(getActivity());
                mEvent.setTime(TimeUtil.time());
            }

        }

        mUserType = getArguments().getInt("userType");
        mTwoPanel = getArguments().getBoolean("twoPanel");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback = (Callback)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.event_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mEvent != null) {
            ((TextView) rootView.findViewById(R.id.time)).setText(DayLogHelper.longDateToTimestamp(mEvent.getTime()));
            rootView.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save();
                }
            });
            mDoctorCommentText = (EditText)rootView.findViewById(R.id.doctorCommentText);
            mDoctorCommentText.setText(mEvent.getDoctorComment());
            if (mUserType == 2) { // is a doctor
                mDoctorCommentText.setEnabled(true);
                mDoctorCommentText.setFocusable(true);
                rootView.findViewById(R.id.commentsSection).setVisibility(View.VISIBLE);
            } else {
                mDoctorCommentText.setEnabled(false);
                if (TextUtils.isEmpty(mEvent.getDoctorComment())) {
                    rootView.findViewById(R.id.commentsSection).setVisibility(View.GONE);
                } else {
                    rootView.findViewById(R.id.commentsSection).setVisibility(View.VISIBLE);
                }
            }
            mNoteText = ((EditText)rootView.findViewById(R.id.noteText));
            mGameText = (TextView)rootView.findViewById(R.id.gameText);
            mReatingAggressionToSelf = (RatingBar)rootView.findViewById(R.id.aggressionToSelf);
            mReatingAggressionToOther = (RatingBar)rootView.findViewById(R.id.aggressionToOther);
            mReatingHappinse= (RatingBar)rootView.findViewById(R.id.happines);
            if (mEvent.getType() == 0) {
                rootView.findViewById(R.id.sectionGame).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.noteSection).setVisibility(View.GONE);
                rootView.findViewById(R.id.measurementSection).setVisibility(View.GONE);
                mGameText.setText(GsonHelper.getJsonFromObject(mEvent.getGame()));
            } else if (mEvent.getType() == 1) {
                rootView.findViewById(R.id.sectionGame).setVisibility(View.GONE);
                rootView.findViewById(R.id.noteSection).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.measurementSection).setVisibility(View.GONE);
                if (!isNewEvent) {
                    mNoteText.setText(mEvent.getNote());
                }

            } else if (mEvent.getType() == 2) {
                rootView.findViewById(R.id.sectionGame).setVisibility(View.GONE);
                rootView.findViewById(R.id.noteSection).setVisibility(View.GONE);
                rootView.findViewById(R.id.measurementSection).setVisibility(View.VISIBLE);
                if (!isNewEvent) {
                    mReatingAggressionToSelf.setRating(mEvent.getMeasurements().getAggressionToSelf());
                    mReatingAggressionToOther.setRating(mEvent.getMeasurements().getAggressionToOther());
                    mReatingHappinse.setRating(mEvent.getMeasurements().getHappiness());
                }
                if (mUserType == 1) {
                    mReatingAggressionToSelf.setEnabled(true);
                    mReatingAggressionToOther.setEnabled(true);
                    mReatingHappinse.setEnabled(true);
                } else {
                    mReatingAggressionToSelf.setEnabled(false);
                    mReatingAggressionToOther.setEnabled(false);
                    mReatingHappinse.setEnabled(false);
                }

            }

        }

        return rootView;
    }

    private void save() {
        if (mUserType == 2) {
            mEvent.setDoctorComment(mDoctorCommentText.getText().toString());
        } else {
            if (mEvent.getType() == 1) { // Note
                mEvent.setNote(mNoteText.getText().toString());
            } else if (mEvent.getType() == 2) { // Measurements
                mEvent.getMeasurements().setAggressionToSelf((int)mReatingAggressionToSelf.getRating());
                mEvent.getMeasurements().setAggressionToOther((int) mReatingAggressionToOther.getRating());
                mEvent.getMeasurements().setHappiness((int) mReatingHappinse.getRating());
            }
        }
        mCallback.saveEvent(mEvent);
    }

    public interface Callback {
        void saveEvent(Event event);
    }
}
