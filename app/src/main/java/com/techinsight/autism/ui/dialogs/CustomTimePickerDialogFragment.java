package com.techinsight.autism.ui.dialogs;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.TimePicker;


import com.techinsight.autism.R;

import java.util.Calendar;

/**
 * Created by alex on 8/20/15.
 */
public class CustomTimePickerDialogFragment extends DialogFragment {

    private OnDateSetListener mCallback;
    private TimePicker mTimePicker;

    public static CustomTimePickerDialogFragment newInstance(long selectedDateMs) {
        CustomTimePickerDialogFragment frag = new CustomTimePickerDialogFragment();
        Bundle args = new Bundle();
        args.putLong("selectedDateMs", selectedDateMs);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        Calendar calendar =  Calendar.getInstance();
        calendar.setTimeInMillis(getArguments().getLong("selectedDateMs"));
        mTimePicker = new TimePicker(getActivity());
        mTimePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        mTimePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(getArguments().getLong("selectedDateMs"));
                calendar.set(Calendar.HOUR_OF_DAY, mTimePicker.getCurrentHour());
                calendar.set(Calendar.MINUTE, mTimePicker.getCurrentMinute());
                mCallback.onDateSet(calendar.getTimeInMillis());
            }
        });
        builder.setNegativeButton("Cancel", null);
        android.support.v7.app.AlertDialog alertDialog = builder.create();
        alertDialog.setView(mTimePicker);
        return alertDialog;
    }

    public void setOnDataSetListener(OnDateSetListener callback){
        mCallback = callback;
    }

    /**
     * The callback interface
     */
    public interface OnDateSetListener {
        void onDateSet(long timeMs);
    }

    public static void showDialog(FragmentManager fragmentManager, long selectedDateMs, OnDateSetListener callback) {
        String dialogTag = CustomTimePickerDialogFragment.class.getSimpleName();
        if (fragmentManager.findFragmentByTag(dialogTag) == null){
            CustomTimePickerDialogFragment dialogFragment = CustomTimePickerDialogFragment.newInstance(selectedDateMs);
            dialogFragment.show(fragmentManager, dialogTag);
            dialogFragment.setOnDataSetListener(callback);
        }
    }

    private void close(){
        dismiss();
    }

}
