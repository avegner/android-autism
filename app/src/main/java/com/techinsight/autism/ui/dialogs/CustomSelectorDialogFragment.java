package com.techinsight.autism.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;


/**
 * Created by alex on 10/4/14.
 */
public class CustomSelectorDialogFragment extends DialogFragment {

    private OnDateSetListener mCallback;

    public static CustomSelectorDialogFragment newInstance(String title, String[] values, int selectedIndex) {
        CustomSelectorDialogFragment frag = new CustomSelectorDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putStringArray("values", values);
        args.putInt("selectedIndex", selectedIndex);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), com.techinsight.autism.R.style.MyAlertDialogStyle);
        builder.setTitle( getArguments().getString("title"))
                .setSingleChoiceItems(getArguments().getStringArray("values"), getArguments().getInt("selectedIndex", 0), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int position) {
                        mCallback.onDateSet(position);
                        close();
                    }
                });
        return builder.create();
    }

    public void setOnDataSetListener(OnDateSetListener callback){
        mCallback = callback;
    }

    /**
     * The callback interface
     */
    public interface OnDateSetListener {
        void onDateSet(int position);
    }

    public static void showDialog(FragmentManager fragmentManager, String title, String[] values, int selectedIndex, OnDateSetListener callback) {
        String dialogTag = CustomSelectorDialogFragment.class.getSimpleName();
        if (fragmentManager.findFragmentByTag(dialogTag) == null){
            CustomSelectorDialogFragment dialogFragment = CustomSelectorDialogFragment.newInstance(title, values, selectedIndex);
            dialogFragment.show(fragmentManager, dialogTag);
            dialogFragment.setOnDataSetListener(callback);
        }
    }

    private void close(){
        dismiss();
    }

}
