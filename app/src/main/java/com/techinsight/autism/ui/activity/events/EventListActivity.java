package com.techinsight.autism.ui.activity.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import com.techinsight.autism.model.Event;
import com.techinsight.autism.ui.activity.BaseActivity;
import com.techinsight.autism.ui.adapter.EventViewAdapter;
import com.techinsight.autism.ui.dialogs.CustomChooserDialogFragment;
import com.techinsight.autism.ui.dialogs.CustomDatePickerDialogFragment;
import com.techinsight.autism.util.DayLogHelper;
import com.techinsight.autism.util.GsonHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * An activity representing a list of Events. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link EventDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class EventListActivity extends BaseActivity implements EventDetailFragment.Callback{

    public static final String USER_TYPE = "USER_TYPE";
    public static final int USER_TYPE_OBSERVER = 1;
    public static final int USER_TYPE_DOCTOR = 2;
    /*
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private EventViewAdapter mAdapter;
    private Date mCurrentDate;
    private Date mPrevCurrentDate;
    private Button mPrevDateButton;
    private Button mNextDateButton;
    private Button mSelectDateButton;
    private ValueEventListener mValueEventListener;

    public static Intent getStartWithDoctorIntent(Context context){
        Intent intent = new Intent(context, EventListActivity.class);
        intent.putExtra(USER_TYPE, USER_TYPE_DOCTOR);
        return intent;
    }

    public static Intent getStartWithObserverIntent(Context context){
        Intent intent = new Intent(context, EventListActivity.class);
        intent.putExtra(USER_TYPE, USER_TYPE_OBSERVER);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.techinsight.autism.R.layout.activity_event_list);

        Toolbar toolbar = (Toolbar) findViewById(com.techinsight.autism.R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(com.techinsight.autism.R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomChooserDialogFragment.showDialog(getSupportFragmentManager(), "Create Event", new String[]{"Note", "Measurements"}, new CustomChooserDialogFragment.OnDateSetListener() {

                    @Override
                    public void onDateSet(int position) {
                        Event event = new Event();
                        if (position == 0) {
                            event.setType(1); // note
                        } else if (position == 1) {
                            event.setType(2); // measurements
                        }
                        showEventDetails(event);

                    }
                });
            }
        });

        View recyclerView = findViewById(com.techinsight.autism.R.id.event_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);

        if (findViewById(com.techinsight.autism.R.id.event_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mPrevDateButton = (Button)findViewById(com.techinsight.autism.R.id.prevDateButton);
        mPrevDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrevCurrentDate = mCurrentDate;
                mCurrentDate = DayLogHelper.getPrevDay(mCurrentDate);
                refreshDate();
            }
        });
        mNextDateButton = (Button)findViewById(com.techinsight.autism.R.id.nextDateButton);
        mNextDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrevCurrentDate = mCurrentDate;
                mCurrentDate = DayLogHelper.getNextDay(mCurrentDate);
                refreshDate();
            }
        });
        mSelectDateButton = (Button)findViewById(com.techinsight.autism.R.id.selectDateButton);
        mSelectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDatePickerDialogFragment.showDialog(getSupportFragmentManager(), mCurrentDate.getTime(), new CustomDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(long timeMs) {
                        mPrevCurrentDate = mCurrentDate;
                        mCurrentDate = new Date(timeMs);
                        refreshDate();
                    }
                });
            }
        });

        Firebase.setAndroidContext(this);
        mCurrentDate = new Date();
        refreshDate();

    }

    private void refreshDate() {
        showProgressDialog();
        mSelectDateButton.setText(DayLogHelper.dayDateToString(mCurrentDate));
        Firebase myFirebaseRef = new Firebase("https://autism.firebaseio.com/EVENTS");

        if (mValueEventListener != null) {
            myFirebaseRef.child(DayLogHelper.dayDateToString(mPrevCurrentDate)).removeEventListener(mValueEventListener);
        } else {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    dismissProgressDialog();

                    List<Event> events = new ArrayList<Event>((int) snapshot.getChildrenCount());

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Event event = postSnapshot.getValue(Event.class);
                        events.add(event);
                    }
                    Collections.sort(events, new EventComparator());
                    mAdapter.updateList(new ArrayList<>(events));
                }

                @Override
                public void onCancelled(FirebaseError error) {
                    dismissProgressDialog();
                }
            };
        }

        /*NestedScrollView scrollView = (NestedScrollView)findViewById(R.id.event_detail_container);
        scrollView.removeAllViews();*/
        Fragment f = getSupportFragmentManager().findFragmentByTag(EventDetailFragment.class.getSimpleName());
        if (f != null) {
            getSupportFragmentManager().beginTransaction().hide(f).commit();
        }


        myFirebaseRef.child(DayLogHelper.dayDateToString(mCurrentDate)).addValueEventListener(mValueEventListener);
    }


    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        mAdapter = new EventViewAdapter(this, new ArrayList<Event>(), new EventViewAdapter.Callback() {
            @Override
            public void showEvent(Event event) {
                showEventDetails(event);
            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(mAdapter);
    }

    private void showEventDetails(Event event) {
        String eventJson = GsonHelper.getJsonFromObject(event);
        if (mTwoPane) {
            EventDetailFragment fragment = EventDetailFragment.newInstance(eventJson,getIntent().getIntExtra(USER_TYPE, 1), true);
            getSupportFragmentManager().beginTransaction().replace(com.techinsight.autism.R.id.event_detail_container, fragment, "EventDetailFragment").commit();
        } else {
            startActivity(EventDetailActivity.getStartIntent(this, eventJson, getIntent().getIntExtra(USER_TYPE, 1), DayLogHelper.dayDateToString(mCurrentDate)));
        }
    }

    @Override
    public void saveEvent(Event event) {
        Firebase myFirebaseRef = new Firebase("https://autism.firebaseio.com/EVENTS/");

        myFirebaseRef
                .child(DayLogHelper.dayDateToString(mCurrentDate))
                .child(event.getId())
                .setValue(event);
    }

    public class EventComparator implements Comparator<Event>
    {
        public int compare(Event left, Event right) {
            return left.getTime() > right.getTime() ? 1 :
                    left.getTime() == right.getTime() ? 0 : -1;
        }
    }


}
