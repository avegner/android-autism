package com.techinsight.autism.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;


/**
 * Created by alex on 10/24/14.
 */
public class CustomProgressDialog extends ProgressDialog {

    /**
     * Get new instance of CustomProgressDialog
     * @param context Activity context
     * @return CustomProgressDialog object
     */
    public static CustomProgressDialog newInstance(Context context) {
        return newInstance(context, null, false);
    }

    /**
     * Get new instance of CustomProgressDialog
     * @param context Activity context
     * @param message Dialog message
     * @param isCancelable Can be canceled
     * @return CustomProgressDialog object
     */
    public static CustomProgressDialog newInstance(Context context, String message, boolean isCancelable) {
        return new CustomProgressDialog(context, message, isCancelable);
    }

    /**
     * Constructor of CustomProgressDialog
     * @param context Activity context
     * @param message Dialog message
     * @param isCancelable Can be canceled
     */
    private CustomProgressDialog(Context context, String message, boolean isCancelable) {
        super(context);
        setCancelable(isCancelable);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.techinsight.autism.R.layout.progress_dialog);
    }
}
