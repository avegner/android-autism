package com.techinsight.autism.ui.activity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;

import com.techinsight.autism.ui.dialogs.CustomProgressDialog;

/**
 * Created by alex on 10/30/15.
 */
public class BaseActivity extends AppCompatActivity {

    private static final int PROGRESS_DIALOG_ID = 1001;

    public void showProgressDialog(){
        if (!isFinishing()) {
            showDialog(PROGRESS_DIALOG_ID);
        }
    }

    public void dismissProgressDialog(){
        doDismissDialog(PROGRESS_DIALOG_ID);
    }

    /**
     * Dismiss Dialog and catch {@link IllegalArgumentException}
     * @param id Dialog id
     */
    private void doDismissDialog(int id){
        try {
            dismissDialog(id);
        } catch (IllegalArgumentException e){}
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case PROGRESS_DIALOG_ID:
                return CustomProgressDialog.newInstance(this);
            default:
                return super.onCreateDialog(id);
        }
    }
}
