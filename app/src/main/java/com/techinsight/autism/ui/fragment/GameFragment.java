package com.techinsight.autism.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.techinsight.autism.R;
import com.techinsight.autism.model.Game;
import com.techinsight.autism.model.HardRate;
import com.techinsight.autism.model.Question;
import com.techinsight.autism.util.LogUtils;
import com.techinsight.autism.util.TimeUtil;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 10/30/15.
 */
public class GameFragment extends BaseFragment implements DataApi.DataListener{

    private static final String START_ACTIVITY = "/START_HEART_RATE";
    private static final String STOP_ACTIVITY = "/STOP_HEART_RATE";

    private static final String TAG = LogUtils.makeLogTag(GameFragment.class);
    private static final String LEVEL = "LEVEL";
    private static final String COUNT_KEY = "com.example.key.count";
    private Game mGame;
    private ImageView mImage;
    private Button mAnswer1Button;
    private Button mAnswer2Button;
    private Button mAnswer3Button;
    private Button mAnswer4Button;
    private GameListener mCallback;
    private GoogleApiClient mGoogleApiClient;
    private View mHardRateSection;
    private TextView mHardRateText;


    public static Fragment newInstance() {
        Fragment fragment = new GameFragment();

        //Bundle args = new Bundle();
        //args.putInt(LEVEL, level);
        //fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.game_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback = (GameListener)getActivity();
        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Game");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImage = (ImageView) getView().findViewById(R.id.image);
        mAnswer1Button = (Button) getView().findViewById(R.id.answer1Button);
        mAnswer1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        mAnswer2Button = (Button) getView().findViewById(R.id.answer2Button);
        mAnswer2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        mAnswer3Button = (Button) getView().findViewById(R.id.answer3Button);
        mAnswer3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        mAnswer4Button = (Button) getView().findViewById(R.id.answer4Button);
        mAnswer4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });

        mGame = Game.makeFake();
        mGame.setStartTime(TimeUtil.time());
        mGame.setCurrentQuestion(-1);
        playNextQuestion();

        mHardRateSection = getView().findViewById(R.id.hartRateSection);
        mHardRateText = (TextView)getView().findViewById(R.id.hartRateText);

        initGoogleApiClient();



    }

    private void playNextQuestion() {
        Question question = mGame.nextQuestion();
        if (question == null) {
            finishGame();
            return;
        }
        question.getQuestionResult().setStartTime(TimeUtil.time());
        mImage.setImageResource(question.getImage());
        mAnswer1Button.setVisibility(View.VISIBLE);
        mAnswer2Button.setVisibility(View.VISIBLE);
        mAnswer3Button.setVisibility(View.VISIBLE);
        mAnswer4Button.setVisibility(View.VISIBLE);
        mAnswer1Button.setText(question.getAnswer1());
        mAnswer2Button.setText(question.getAnswer2());
        mAnswer3Button.setText(question.getAnswer3());
        mAnswer4Button.setText(question.getAnswer4());
    }

    private void finishGame() {
        getActivity().onBackPressed();
        mCallback.endGame(mGame);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                        sendMessage(START_ACTIVITY, "sdf");
                        Wearable.DataApi.addListener(mGoogleApiClient, GameFragment.this);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                .addApi(Wearable.API)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

/*    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        stopHardRate();
        super.onPause();
    }*/

    @Override
    public void onStop() {
        super.onStop();
        stopHardRate();

    }


    public interface GameListener {
        void endGame(Game game);
    }

    private void checkAnswer(View v) {
        if (!mGame.isCompeted()) {
            if (((Button) v).getText().toString().toLowerCase().equals(mGame.question().getCorrectAnswer().toLowerCase())) {
                mGame.question().getQuestionResult().incTriesCount();
                playNextQuestion();
            } else {
                mGame.question().getQuestionResult().incTriesCount();
                showCustomToast();
                v.setVisibility(View.INVISIBLE);
            }
        } else {
            finishGame();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showCustomToast() {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout_id));

        // set a message
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText("Failed, try again!");

        // Toast...
        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), path, text.getBytes() ).await();
                }

            }
        }).start();
    }

    private void stopHardRate() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                String text = "";
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
                for (Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mGoogleApiClient, node.getId(), STOP_ACTIVITY, text.getBytes()).await();
                }
                Wearable.DataApi.removeListener(mGoogleApiClient, GameFragment.this);
                mGoogleApiClient.disconnect();
            }
        }).start();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/TYPE_HEART_RATE") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    int value = dataMap.getInt("value");
                    long time = dataMap.getLong("time");
                    if (mGame != null) {
                        List<HardRate> hardRates = mGame.getHardRateList();
                        if (hardRates == null) {
                            hardRates = new ArrayList<HardRate>();
                            mGame.setHardRateList(hardRates);
                        }
                        hardRates.add(new HardRate(value, time));
                        LogUtils.LOGD("TYPE_HEART_RATE", String.valueOf(value));
                        if (mHardRateSection.getVisibility() == View.INVISIBLE) {
                            mHardRateSection.setVisibility(View.VISIBLE);
                        }
                        mHardRateText.setText(String.valueOf(value));
                    }

                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                // DataItem deleted
            }
        }
    }
}
