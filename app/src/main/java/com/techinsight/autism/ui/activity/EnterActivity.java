package com.techinsight.autism.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.techinsight.autism.R;
import com.techinsight.autism.ui.activity.events.EventListActivity;
import io.fabric.sdk.android.Fabric;

/**
 * Created by alex on 10/30/15.
 */
public class EnterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.enter_acticity);
        Button userButton = (Button) findViewById(R.id.userButton);
        userButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(UserActivity.getStartIntent(EnterActivity.this));
            }
        });

        Button observerButton = (Button) findViewById(R.id.observerButton);
        observerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(EventListActivity.getStartWithObserverIntent(EnterActivity.this));
            }
        });

        Button doctorButton = (Button) findViewById(R.id.doctorButton);
        doctorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(EventListActivity.getStartWithDoctorIntent(EnterActivity.this));
            }
        });
    }
}
