package com.techinsight.autism.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techinsight.autism.R;
import com.techinsight.autism.model.Event;
import com.techinsight.autism.util.DayLogHelper;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by alex on 9/12/15.
 */
public class EventViewAdapter extends RecyclerView.Adapter<EventViewAdapter.EventViewHolder>{


    private final Callback mCallback;
    private final DateFormat mDateFormatter;
    private List<Event> mItemList;
    private Context mContext;

    public EventViewAdapter(Context context, List<Event> itemList, Callback callback) {
        super();
        mItemList = itemList;
        mContext = context;
        mCallback = callback;
        mDateFormatter = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_card_event, viewGroup, false);
        EventViewHolder pvh = new EventViewHolder(v);


        return pvh;
    }

    @Override
    public void onBindViewHolder(EventViewHolder pvh, int position) {
        Event event = mItemList.get(position);
        pvh.event = event;
        pvh.time.setText(DayLogHelper.longDateToTimestamp(pvh.event.getTime()));
        pvh.title.setText(Event.TYPE_STRINGS[pvh.event.getType()]);
        if (TextUtils.isEmpty(pvh.event.getDoctorComment())) {
            pvh.doctorSection.setVisibility(View.GONE);
        } else {
            pvh.doctorSection.setVisibility(View.VISIBLE);
            pvh.doctorCommentText.setText(pvh.event.getDoctorComment());
        }
        pvh.cv.setTag(pvh);

        pvh.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventViewHolder pvh = (EventViewHolder)v.getTag();
                mCallback.showEvent(pvh.event);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void updateList(ArrayList<Event> events) {
        mItemList = events;
        notifyDataSetChanged();
    }

    public static class EventViewHolder extends RecyclerView.ViewHolder {
        public TextView time;
        public TextView title;
        public View doctorSection;
        public TextView doctorCommentText;
        CardView cv;
        Event event;

        EventViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card);
            time = (TextView)itemView.findViewById(R.id.time);
            title = (TextView)itemView.findViewById(R.id.title);
            doctorSection = itemView.findViewById(R.id.doctorSection);
            doctorCommentText = (TextView)itemView.findViewById(R.id.doctorCommentText);
        }
    }

    public interface Callback {
        void showEvent(Event person);
    }
}
